mod opts;
#[cfg(test)]
mod tests;

use std::io::{self, BufWriter, Read, Write};

/// run demogrep on stdin and output to stdout.
fn main() {
    let opts = opts::Opts::parse().unwrap();
    if opts.verbose {
        eprintln!("verbose mode on");
        eprintln!("options: {:?}", opts);
    }
    // io::stdin(), io::stdout(), and io::stderr() use a mutex by default to be concurrency-safe.
    // since we know that our program is single-threaded, we can get a performance improvement by passing an exclusive lock instead.
    // we don't need to do this and could use stdin, out, and err directly if we preferred.
    let (stdin, stdout, stderr) = (io::stdin(), io::stdout(), io::stderr());
    match run(opts, stdin.lock(), stdout.lock(), stderr.lock()) {
        // this isn't necessary: in modern rust, we could just have main() return a Result<(), E>
        // and the compiler will do the rest, but I wanted to show how it works.
        Ok(()) => std::process::exit(0),
        Err(err) => {
            eprintln!("{}", err);
            std::process::exit(1)
        }
    }
}

/// run demogrep on the specified reader and writers.
fn run(
    o: opts::Opts,
    mut stdin: impl Read,
    mut stdout: impl Write,
    // we don't actually use stderr here, but in a more complicated example we'd log to it.
    mut _stderr: impl Write,
) -> std::io::Result<()> {
    let (before, after) = o.context;
    let mut s = String::with_capacity(1024); //
    stdin.read_to_string(&mut s)?;
    let lines: Vec<&str> = s.lines().collect();
    let mut to_write: Vec<bool> = vec![false; lines.len()];
    for (i, line) in lines.iter().enumerate() {
        if !o.matcher.match_str(line) {
            continue;
        }
        let start = usize::saturating_sub(i, before);
        let end = usize::min(i + after, lines.len() - 1);
        for j in start..=end {
            to_write[j] = true;
        }
    }

    let mut stdout = BufWriter::new(stdout);
    if o.line_numbers {
        for (i, line) in lines.iter().enumerate() {
            if to_write[i] {
                writeln!(stdout, "{}\t{}", i, line)?;
            }
        }
    } else {
        for (i, line) in lines.iter().enumerate() {
            if to_write[i] {
                writeln!(stdout, "{}", line)?;
            }
        }
    }
    stdout.flush()
}
