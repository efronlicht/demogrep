//! command-line options
use clap::Clap;
use regex::RegexBuilder;

/// A limited-functionality verison `grep` for demonstration purposes.
/// See the struct opts::Opts for information on fully parsed options and modes.
#[clap(
    version = "0.0.1",
    author = "Efron Licht (Efron.A.Licht@aexp.com)",
    name = "demogrep"
)]
#[derive(Clap, Clone, Debug, PartialEq)]
struct RawOpts {
    #[clap(short = 'B', long = "before", default_value = "0")]
    /// how many lines of context [B]efore the match to display.
    before_context: usize,

    #[clap(short = 'A', long = "after", default_value = "0")]
    /// how many lines of context [A]fter the match to display.
    after_context: usize,
    #[clap(short = 'C', long, default_value = "0")]
    /// how many lines of [C]ontext around the match to display. --context N is equivalent to --before N --after N
    context: usize,

    #[clap(short, long)]
    /// verbose debug info.
    verbose: bool,

    #[clap(short = 'e', long = "string-literal")]
    /// Match text using a string lit[e]ral rather than a regular expression. Can't be used with --lookaround.
    string_literal: bool,

    #[clap(short = 'I', long = "ignore-case")]
    /// [I]gnore case while matching text.
    ignore_case: bool,

    /// the pattern to match, usually as a regexp. see --string-literal, --ignore-case
    pattern: String,

    #[clap(short = 'L', long = "line-numbers")]
    /// Print the [L]ine numbers.
    line_numbers: bool,
}

#[derive(Clone, Debug)]
#[non_exhaustive]
/// Fully parsed and validated options and modes.
/// This struct should always be valid.
/// See [RawOpts] for the actual flags.
pub struct Opts {
    /// lines of context before and after the expression to print.
    pub context: (usize, usize),
    /// the kind of text matching. this is determined by [RawOpts].string_literal and [RawOpts].ingore_case.
    pub matcher: TextMatcher,
    /// verbose debug info.
    pub verbose: bool,
    /// Print the line numbers.
    pub line_numbers: bool,
}
#[derive(Clone, Debug)]
#[non_exhaustive]
// problems with command-line arguments.
pub enum Error {
    BadArg(&'static str),
    Regexp(regex::Error),
}

impl From<regex::Error> for Error {
    fn from(err: regex::Error) -> Self {
        Self::Regexp(err)
    }
}

impl Opts {
    /// parse and validate the command-line options and flags.
    pub fn parse() -> Result<Self, Error> {
        let RawOpts {
            before_context,
            after_context,
            context,
            string_literal,
            ignore_case,
            pattern,
            verbose,
            line_numbers,
        } = RawOpts::parse();
        let opts = Opts {
            verbose,
            line_numbers,
            context: match (before_context, after_context, context) {
                (0, 0, c) => (c, c),
                (b, a, 0) => (b, a),
                _ => {
                    return Err(Error::BadArg(
                        "options -B and (-C or -A) are mutually exclusive",
                    ))
                }
            },
            matcher: match (string_literal, ignore_case) {
                (true, true) => TextMatcher::StringLiteralCaseInsensitive(pattern.to_lowercase()),
                (true, false) => TextMatcher::StringLiteral(pattern),
                _ => TextMatcher::Regexp(
                    RegexBuilder::new(&pattern)
                        .case_insensitive(ignore_case)
                        .build()?,
                ),
            },
        };
        Ok(opts)
    }
}

#[non_exhaustive]
#[derive(Clone, Debug)]
pub enum TextMatcher {
    Regexp(regex::Regex),
    StringLiteral(String),
    StringLiteralCaseInsensitive(String),
}

impl TextMatcher {
    pub fn match_str(&self, text: &str) -> bool {
        match self {
            // technically cheating with the lookarounds. don't mind me.
            TextMatcher::Regexp(re) => re.is_match(text),
            TextMatcher::StringLiteral(s) => text.contains(s),
            TextMatcher::StringLiteralCaseInsensitive(s) => text.to_lowercase().contains(s),
        }
    }
}
